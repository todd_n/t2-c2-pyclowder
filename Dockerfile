FROM ubuntu:14.04
MAINTAINER Rui Liu <ruiliu@illinois.edu>

COPY pyclowder /tmp/pyclowder/pyclowder
COPY setup.py /tmp/pyclowder

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    python \
    python-pip && \
    rm -rf /var/lib/apt/lists/* && \
    pip install --upgrade pika && \
    pip install --upgrade requests && \
    pip install --upgrade /tmp/pyclowder && \
    rm -rf /tmp/pyclowder && \
    adduser --system clowder

USER clowder
