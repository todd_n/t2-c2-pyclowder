#!/usr/bin/env python
import subprocess
import logging
import sys
from config import *
import pyclowder.hpc.extractors as extractors


def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL

    # Set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowderhpc.extractors').setLevel(logging.INFO)

    picklefile = str(sys.argv[1])
    # Prepare for processing in an HPC environment
    extractors.prepare_hpc_processing(extractorName=extractorName, messageType=messageType,
                                      processFileFunction=process_file, picklefile=picklefile)


# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):
    global extractorName
    
    inputfile = parameters['inputfile']

    # Call actual program
    result = subprocess.check_output(['wc', inputfile], stderr=subprocess.STDOUT)
    (lines, words, characters, filename) = result.split()

     # context url
    context_url = 'https://clowder.ncsa.illinois.edu/clowder/contexts/metadata.jsonld'

    # store results as metadata
    metadata = {
                '@context': [context_url, 
                    {'lines': 'http://clowder.ncsa.illinois.edu/'+ extractorName +'#lines',
                     'words':'http://clowder.ncsa.illinois.edu/'+ extractorName + '#words',
                     'characters':'http://clowder.ncsa.illinois.edu/'+ extractorName + '#characters'}],
                'attachedTo': {'resourceType': 'file', 'id': parameters["fileid"]},
                'agent': {
                    '@type': 'cat:extractor',
                    'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                },
                'content': {'lines': lines,
                            'words': words,
                            'characters': characters
                            }
                }
    print metadata
    # upload metadata
    extractors.upload_file_metadata_jsonld(mdata=metadata, parameters=parameters)            


if __name__ == "__main__":
    main()
