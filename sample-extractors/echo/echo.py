#!/usr/bin/env python

import logging
from config import *
import pyclowder.extractors as extractors

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger, registrationEndpoints

    # set logging
    logging.basicConfig(format='%(asctime)-15s %(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)
    logger = logging.getLogger('extractor')
    logger.setLevel(logging.DEBUG)

    # setup
    extractors.setup(extractorName=extractorName,
                       messageType=messageType,
                       rabbitmqURL=rabbitmqURL,
                       rabbitmqExchange=rabbitmqExchange)

    # register extractor info
    extractors.register_extractor(registrationEndpoints)

    # connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName,
                                   messageType=messageType,
                                   rabbitmqURL=rabbitmqURL,
                                   rabbitmqExchange=rabbitmqExchange,
                                   processFileFunction=process_file, 
                                   checkMessageFunction=None)


# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):
    global extractorName
    
    # store results as metadata
    rabbitmq={}
    for key, value in parameters.items():
        if key not in ('channel', 'header', 'secretKey'):
            rabbitmq[key]=value
     # context url
    context_url = 'https://clowder.ncsa.illinois.edu/clowder/contexts/metadata.jsonld'

    # store results as metadata
    metadata = {
                '@context': [context_url, 
                    {'rabbitmq': 'http://clowder.ncsa.illinois.edu/'+ extractorName +'#rabbitmq'}
                    ],
                'attachedTo': {'resourceType': 'file', 'id': parameters["fileid"]},
                'agent': {
                    '@type': 'cat:extractor',
                    'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/'+ extractorName
                },
                'content': {'rabbitmq': rabbitmq
                            }
                }
    print metadata
    # upload metadata
    extractors.upload_file_metadata_jsonld(mdata=metadata, parameters=parameters)            

    return False

if __name__ == '__main__':
    main()
