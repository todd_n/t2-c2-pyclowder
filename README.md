# pyClowder

*pyClowder* is a python module that can be used to easily write extractors that interact with Clowder. To install the package, run:

`pip install git+https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git`

or download the source code and then, from the terminal, run:

`python setup.py install`

Sample extractors can be found on the *sample-extractors* subfolder.

# Building docker images

You can run the docker.sh script to create docker images of both a base pyclowder image, as well as an example extractor as a docker image. The docker.sh script takes a few environment variables as arguments:

- VERBOSE=${VERBOSE:-""}
- PUSH : if set to any value and PROJECT is set, the script will push the images to hub.docker.com
- PROJECT : if set this wil be used as the project of the image name, if not set it will default to ncsa.
- VERSION=${VERSION:-""}

The two images created are:
- $PROJECT/clowder-extractors-python-base : This is the base image.
- $PROJECT/clowder-extractors-wordcount : This is an example extractor. This is based on ncsa/clowder-extractors-python-base. If the PROJECT is not set to ncsa, it will pull the latest image from hub.docker.com.

