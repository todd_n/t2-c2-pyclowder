import os
import json
import logging
import requests

# ----------------------------------------------------------------------
# find/create the collection in Clowder
# ----------------------------------------------------------------------
def get_collection(clowder_url, secret_key, collection, description=''):
  """Finds or creats a collection in Clowder.

  If the collectin with the given name does not exist yet, it will
  be created. In both cases the collection is returned.

  Args:
      clowder_url: url of Clowder, should end with a /.
      secret_key: key to access Clowder.
      collection: name of collection to find/create.
      description: used when creating the collection.

  Returns:
      The collection found/created or None if it could not be created.

  Raises:
      HttpException: An error occured accessing Clowder.
  """
  log = logging.getLogger(__name__)

  # check to see if collection exists
  # TODO FUNCTION SHOULD NOT USE LIST
  r = requests.get('%sapi/collections/list?key=%s' %
                   (clowder_url, secret_key))
  if (r.status_code != 200):
    log.error('Could not get list of collections : [%d] - %s)' %
              (r.status_code, r.text))
    return None
  result = r.json()
  if isinstance(result, list):
    for c in result:
      if c['name'] == collection:
        return c

  # create the collection
  log.debug('Creating collection "%s".' % collection)
  headers = {'Content-type': 'application/json'}
  body={'name': collection, 'description': description}
  r = requests.post('%sapi/collections?key=%s' %
                    (clowder_url, secret_key),
                    data=json.dumps(body), headers=headers)
  if (r.status_code != 200):
    log.error('Problem creating collection  : [%d] - %s)' %
              (r.status_code, r.text))
    return None
  collection_id = r.json()['id']

  # add for the next call
  return { "id": collection_id, "name": collection, "description": description}
  # fetch collection and return
  # TODO FUNCTION DOES NOT EXIST
  #r = requests.get('%sapi/collections/%s?key=%s' %
  #                 (clowder_url, collection_id, secret_key))
  #if (r.status_code != 200):
  #  log.error('Could not get the collection : [%d] - %s)' %
  #            (r.status_code, r.text))
  #  return None
  #return r.json()

# ----------------------------------------------------------------------
# upload file to clowder, this is needed to create dataset
# ----------------------------------------------------------------------
def upload_file(clowder_url, secret_key, filename):
  """Uploads a file to clowder.

  The file will be uploaded and the id of the file in clowder is
  returned.

  Args:
      clowder_url: url of clowder, should end with a /.
      secret_key: key to access clowder.
      filename: path to file on disk.

  Returns:
      The id of the file uploaded or None if it failed.

  Raises:
      HttpException: An error occured accessing clowder.
      IOException: If the filename could not be found.
  """
  log = logging.getLogger(__name__)

  r = requests.post('%sapi/files?key=%s' % (clowder_url, secret_key),
                    files={'File' : open(filename, 'rb')})
  if (r.status_code != 200):
    log.error('Problem uploading file  : [%d] - %s)' %
              (r.status_code, r.text))
    return None
  file_id = r.json()['id']

  # fetch file and return
  # TODO does not work see MMDB-1685
  # r = requests.get('%sapi/files/%s?key=%s' %
  #                  (clowder_url, file_id, secret_key))
  # if (r.status_code != 200):
  #   log.error('Could not get the file : [%d] - %s)' %
  #             (r.status_code, r.text))
  #   return None
  # return r.json()
  return file_id

# ----------------------------------------------------------------------
# create a new dataset in clowder and return ID
# ----------------------------------------------------------------------
def create_dataset(clowder_url, name, description, username=None, password=None, secret_key=None):
    """
    Create an empty dataset with given name and description, and return ID.

    :param clowder_url: host URL of target Clowder instance
    :param name: name of Dataset to create
    :param description: description of Dataset to create
    :param username: Clowder username
    :param password: Clowder password
    :param secret_key: API access key can be provided instead of username & password
    :return: ID of dataset that was created
    """

    log = logging.getLogger(__name__)

    # Prepare URL based on auth method
    sess = requests.Session()
    if (secret_key) and not (username and password):
        url_path = '%sapi/datasets/createempty?key=%s' % (clowder_url, secret_key)
    elif (username and password):
        sess.auth = (username, password)
        url_path = '%sapi/datasets/createempty' % (clowder_url)
    else:
        print("Please provide either user credentials or an API access key.")
        return

    r = sess.post(url_path, headers={"Content-Type":"application/json"},
                  data='{"name":"%s", "description":"%s"}' % (name, description))

    if (r.status_code != 200):
        log.error('Problem creating dataset  : [%d] - %s)' % (r.status_code, r.text))
        return None
    ds_id = r.json()['id']

    return ds_id

# ----------------------------------------------------------------------
# upload files with optional metadata to existing dataset in clowder
# ----------------------------------------------------------------------
def upload_files_to_dataset(clowder_url, dataset_id, filename_list, metadata={}, username=None, password=None, secret_key=None):
    """
    Upload one or more files to existing dataset, attaching metadata if available.

    Example of uploading 2 files, one with metadata:
        upload_files_to_dataset("http://localhost:9000/", "56747735ea64bf1720266346", ["C:/file1.txt", "C:/file2.txt"],
                                {"file.txt": {"metadata1": 500, "metadata two":"value"}}, "username", "pass")

    :param clowder_url: host URL of target Clowder instance
    :param dataset_id: ID of dataset to attach files to
    :param filename_list: list of filenames to upload e.g. ["MyFile.txt", "MyFile2.txt"]
    :param metadata: JSON object containing metadata; key is filename and value is a JSON metadata object
    :param username: Clowder username
    :param password: Clowder password
    :param secret_key: API access key can be provided instead of username & password
    :return: list of IDs of uploaded files
    """

    log = logging.getLogger(__name__)

    # Prepare the files to be sent
    files_to_send = {}
    for filename in filename_list:
        if not os.path.isfile(filename):
            continue
        f = open(filename, 'rb')
        # for key in body.files, use filename with path omitted
        files_to_send[os.path.basename(filename)] = f

    # Make sure metadata is encoded in proper format for each file
    for md in metadata:
        md_obj = metadata[md]
        # Json metadata for each file's key should be wrapped in a list
        if type(md_obj) is not list:
            if type(md_obj) is not str:
                metadata[md] = [json.dumps(md_obj)]
            else:
                metadata[md] = [md_obj]
        else:
            if type(md_obj) is not str:
                metadata[md] = json.dumps(md_obj)

    # Prepare URL based on auth method
    sess = requests.Session()
    if (secret_key) and not (username and password):
        url_path = '%sapi/uploadToDataset/%s?key=%s' % (clowder_url, dataset_id, secret_key)

    elif (username and password):
        sess.auth = (username, password)
        url_path = '%sapi/uploadToDataset/%s' % (clowder_url, dataset_id)
    else:
        print("Please provide either user credentials or an API access key.")
        return

    r = sess.post(url_path, files=files_to_send, data=metadata)

    if (r.status_code != 200):
        log.error('Problem attaching files  : [%d] - %s)' % (r.status_code, r.text))
        return None

    # Return single ID if single file, or all IDs + filenames if multiple files
    resp_json = r.json()
    if 'id' in resp_json:
        return resp_json['id']
    else:
        return resp_json['ids']


# ----------------------------------------------------------------------
